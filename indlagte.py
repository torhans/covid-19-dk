#!/usr/bin/python3
import urllib3
from bs4 import BeautifulSoup as bs
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Qt5Agg')
import numpy as np
http = urllib3.PoolManager()
r=http.request('GET',"https://www.sst.dk/da/corona/tal-og-overvaagning")
soup=bs(r.data)

def istable(tag):
    return (tag.name=='h3') and (tag.text=='Indlagte patienter med bekræftet COVID-19 på intensivafdeling og i respirator')

rows=soup.find(istable).find_next('table').find_all('tr')
tbl=[]
for row in rows[2:]:
    cols=row.find_all('td')
    rd=[]
    for col in cols[1:]:
        try:
            rd.append(int(col.text))
        except:
            rd.append(0)
    tbl.append(rd)
ntbl=np.array(tbl)
d=np.arange(16,32)
plt.semilogy(d,ntbl[::-1,-1])
plt.grid()
plt.ylabel("Antal indlagte")
plt.xlabel("Dato i marts")
plt.show()
